/*
	smb explorer browser.js
*/

// extensions
var smbexplorer_ext_arr = ['txt', 'img'];
smbexplorer_ext_arr['txt'] = ['txt'];
smbexplorer_ext_arr['img'] = ['jpg','png','gif','peg'];
  

$(document).ready(function () {
    smbexplorer_onloadfn(); // I think this is good practice
});

function smbexplorer_onloadfn() {
	smbexplorer_loaddir('');
}

function smbexplorer_loaddir(path) {
	$('#smbexplorer_browser').html('<img src="' + smbexplorer_webbase + 'images/loader_browser.gif" alt="loading"/>');
	$('#smbexplorer_browser').load('smbexplorer/' + smbexplorer_server_nr + '/ajax/getdir' , { path : path } , function(){
	  smbexplorer_loaddir_onload(path);
 	});
}

function smbexplorer_loaddir_onload(path){
  $(".smbexplorer_browser_dir").click(function (e) { 
		smbexplorer_browser_menu(e, path, $(this).attr('id'),'dir');
  });	
  
  $(".smbexplorer_browser_file").click(function (e) { 
  	smbexplorer_browser_menu(e, path, $(this).attr('id'),'file');
  });	
  
  $(".smbexplorer_browser_up").click(function () { 
    var path_arr = path.split("/");
    smbexplorer_loaddir(path.substring(0,path.length - path_arr[path_arr.length - 1].length - 1));
  });
}

function smbexplorer_getfile(path, download) {
	$('#smbexplorer_editor_title').html(smbexplorer_server_name + ' ' + path);
	
	var content = '';
	content += '<img onClick="$(\'#smbexplorer_editor\').slideUp(\'fast\'); smbexplorer_getfile(\'' + path + '\', true);" src="' + smbexplorer_webbase + 'images/save.png" alt="download" />';
	content += '<img onClick="$(\'#smbexplorer_editor\').slideUp();" src="' + smbexplorer_webbase + 'images/editor_close.png" alt="close"/>';
	$('.smbexplorer_editor_controls').html(content);	
	
	$('#smbexplorer_editor_file').html('<img src="' + smbexplorer_webbase + 'images/loader_browser.gif" alt="loading"/>');
	$('#smbexplorer_editor').slideDown();
	
	var path_arr = path.split(".");
  var ext = path_arr[path_arr.length - 1];

	if (download == true) {
		document.location = 'smbexplorer/' + smbexplorer_server_nr + '/ajax/getfile/true/' + path;
		$('#smbexplorer_editor').slideUp();
	}	

  else if(smbexplorer_ext_arr['txt'].indexOf(ext.toLowerCase()) > -1) {
  	// save
  	$('#smbexplorer_editor_file').load('smbexplorer/' + smbexplorer_server_nr + '/ajax/getfile/false' , { path : path } , function(){
			$('#smbexplorer_editor').slideDown();
		});
	}
	else if(smbexplorer_ext_arr['img'].indexOf(ext.toLowerCase()) > -1) {
		$('#smbexplorer_editor_file').html('<img src="smbexplorer/' + smbexplorer_server_nr + '/ajax/getfile/false' + path + '" alt="file_preview" class="smbexplorer_editor_img"/>');
		$('#smbexplorer_editor').slideDown();
 	}
	else {
		document.location = 'smbexplorer/' + smbexplorer_server_nr + '/ajax/getfile/true' + path;
		$('#smbexplorer_editor').slideUp();
	}
}

function smbexplorer_browser_menu(e, path, file, type){
	var menucontent = '';
	
	// open directory
	if (type == 'dir') menucontent += '<img onClick="$(this).parent().fadeOut(\'fast\'); smbexplorer_loaddir(\'' + path + '/' + file + '\');" src="' + smbexplorer_webbase + 'images/dir_open.png" alt="open" class="smbexplorer_browser_imgtip" />';
	
	// open directory
	if (type == 'file') {
		var path_arr = file.split(".");
  	var ext = path_arr[path_arr.length - 1];

		// if we can, offer preview
		if((smbexplorer_ext_arr['txt'].indexOf(ext.toLowerCase()) > -1) || (smbexplorer_ext_arr['img'].indexOf(ext.toLowerCase()) > -1)) {
			menucontent += '<img onClick="$(this).parent().fadeOut(\'fast\'); smbexplorer_getfile(\'' + path + '/' + file + '\', false);" src="' + smbexplorer_webbase + 'images/preview.png" alt="Preview" class="smbexplorer_browser_imgtip" />';
		} 
		
		// save
		menucontent += '<img onClick="$(this).parent().fadeOut(\'fast\'); smbexplorer_getfile(\'' + path + '/' + file + '\', true);" src="' + smbexplorer_webbase + 'images/save.png" alt="Download" class="smbexplorer_browser_imgtip" />';
		
	}
	
	
	// close button
	menucontent += '<img onClick="$(this).parent().fadeOut(\'fast\')" src="' + smbexplorer_webbase + 'images/editor_close.png" alt="Close" class="smbexplorer_browser_imgtip" />';
	
	$(".smbexplorer_browser_menu").html(menucontent);
	$(".smbexplorer_browser_menu").css('top',e.pageY - 20).css('left',e.pageX -20).fadeIn('fast');
}

function smbexplorer_login(path) {
  var post_arr = new Array();
  
  post_arr[0] = new Array();
  post_arr[0]['name'] = "path";
  post_arr[0]['value'] = path;
  
  post_arr[1] = new Array();
  post_arr[1]['name'] = "user";
  post_arr[1]['value'] = $("#smbexplorer_user").attr('value');
  
  post_arr[2] = new Array();
  post_arr[2]['name'] = "pass";
  post_arr[2]['value'] = $("#smbexplorer_pass").attr('value'); 
  
  $('#smbexplorer_browser').html('<img src="' + smbexplorer_webbase + 'images/loader_browser.gif" alt="loading"/>');
	
  $('#smbexplorer_browser').load('smbexplorer/' + smbexplorer_server_nr + '/ajax/login' , post_arr , function(){
    smbexplorer_loaddir_onload(path);
  }); 
}
