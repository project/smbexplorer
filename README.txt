

-- SUMMARY --

SMB Explorer module is AJAX based file manager module for Samba (Windows Network) shares. 
Client part, samba stream, is based on smb4php class (http://www.phpclasses.org/smb4php), 
made by Victor M. Varela <vmvarela@gmail.com>.


-- DEPENDENCIES --

smb.php relies on smbclient executable. You have to install this on your server.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

You can add/manage/delete samba servers and their options on page "admin/settings/smbexplorer"


-- USAGE --

after configuration, browse page "smbexplorer"


-- CONTACT --

Current maintainers:
* Janis Bebritis (Jancis) - jancis@iists.it

